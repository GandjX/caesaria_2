var g_config = {}

g_config.widget = {}

g_config.level = {
    res_menu: 0,
    res_restart: 2
}

g_config.lobby = {
    res_close:5
}

g_config.align = {
  upperLeft:0,
  lowerRight:1,
  center:2,
  scale:3,
  automatic:4
}

g_config.audio = {
    theme: 2,
    speech: 4,
    effects: 5,
    infobox : 6
}

g_config.climate = {
    central: 0,
    northen: 1,
    desert: 2
}

g_config.gift = {
    modest: 1,
    generous: 2,
    lavish: 3
}

g_config.rc = {
    panel : "paneling",
    invalidIndex : 567
}

g_config.advisor = {
    none: 0,
    employers: 1,
    military: 2,
    empire: 3,
    ratings: 4,
    trading: 5,
    population: 6,
    health: 7,
    education: 8,
    entertainment: 9,
    religion: 10,
    finance: 11,
    main: 12,
    unknown: 13
}

g_config.walker = {}

g_config.layer = {
    simple : 0,
    water : 1,
    fire : 2,
    damage : 3,
    desirability : 4,
    entertainments : 5,
    entertainment : 6,
    theater : 7,
    amphitheater : 8,
    colosseum : 9,
    hippodrome : 10,
    health : 11,
    healthAll : 12,
    doctor : 13,
    hospital : 14,
    barber : 15,
    baths : 16,
    food : 17,
    religion : 18,
    risks : 19, crime : 20, aborigen : 21, troubles : 22,
    educations : 23, education : 24, school : 25, library : 26, academy : 28,
    commerce : 29, tax : 30, market : 31, sentiment : 32, unemployed : 33, comturnover : 34,
    build : 35, destroyd : 36, constructor : 37,
    products : 38
}

g_config.gods = {
  ceres : "ceres",
  mars : "mars",
  neptune : "neptune",
  venus : "venus",
  mercury : "mercury"
}

g_config.saves = {
    ext : ".oc3save",
    fast : "_fastsave",
    auto : "_autosave",
}
